<?php

namespace Drupal\foundation_layouts_ui\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines the configuration form.
 */
class FoundationLayoutsSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'foundation_layouts_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['foundation_layouts.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('foundation_layouts.settings');
    $breakpoints = $config->get('breakpoints');

    $form['instructions'] = [
      '#markup' => $this->t("Select the breakpoints that should be included in each layout configuration."),
    ];

    $form['small'] = [
      '#title' => $this->t('Small'),
      '#type' => 'checkbox',
      '#default_value' => $breakpoints['small'] ?? FALSE,
    ];

    $form['medium'] = [
      '#title' => $this->t('Medium'),
      '#type' => 'checkbox',
      '#default_value' => $breakpoints['medium'] ?? FALSE,
    ];

    $form['large'] = [
      '#title' => $this->t('Large'),
      '#type' => 'checkbox',
      '#default_value' => $breakpoints['large'] ?? FALSE,
    ];

    $form['xlarge'] = [
      '#title' => $this->t('Extra Large'),
      '#type' => 'checkbox',
      '#default_value' => $breakpoints['xlarge'] ?? FALSE,
    ];

    $form['xxlarge'] = [
      '#title' => $this->t('Extra Extra Large'),
      '#type' => 'checkbox',
      '#default_value' => $breakpoints['xxlarge'] ?? FALSE,
    ];

    $selected_sizes = $config->get('available_sizes') ?? [];
    $available_sizes = array_keys(array_filter($selected_sizes, function($a) {
      return $a != FALSE;
    }));
    $all_sizes = $this->getDefaultSizes();

    $form['available_sizes'] = [
      '#title' => $this->t('Choose the available column sizes.'),
      '#type' => 'checkboxes',
      '#default_value' => $available_sizes,
      '#options' => $all_sizes,
    ];

    $selected_offsets = $config->get('available_offsets') ?? [];
    $all_offsets = $this->getDefaultOffsets();

    $form['available_offsets'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Choose the available column offsets.'),
      '#tree' => TRUE,
    ];

    foreach ($all_offsets as $key => $value) {
      $form['available_offsets'][$key] = [
        '#type' => 'checkbox',
        '#title' => $value,
        '#default_value' => isset($selected_offsets[$key]) ?? (bool) $selected_offsets[$key],
      ];
    }
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $selected_offsets = [];
    $offsets = $form_state->getValue('available_offsets');
    foreach ($offsets as $key => $selected) {
      $selected_offsets[$key] = (bool) $selected;
    }
    $this->config('foundation_layouts.settings')
      ->set('breakpoints.small', $form_state->getValue('small'))
      ->set('breakpoints.medium', $form_state->getValue('medium'))
      ->set('breakpoints.large', $form_state->getValue('large'))
      ->set('breakpoints.xlarge', $form_state->getValue('xlarge'))
      ->set('breakpoints.xxlarge', $form_state->getValue('xxlarge'))
      ->set('available_sizes', $form_state->getValue('available_sizes'))
      ->set('available_offsets', $selected_offsets)
      ->save();
    parent::submitForm($form, $form_state);
  }

  /**
   * Get default sizes.
   *
   * @return array
   *   An array of default sizes.
   */
  public function getDefaultSizes() : array {
    // Column sizes.
    return [
      'auto' => 'Auto',
      1 => '1 Column',
      2 => '2 Columns',
      3 => '3 Columns',
      4 => '4 Columns',
      5 => '5 Columns',
      6 => '6 Columns',
      7 => '7 Columns',
      8 => '8 Columns',
      9 => '9 Columns',
      10 => '10 Columns',
      11 => '11 Columns',
      12 => '12 Columns',
    ];
  }

  /**
   * Get defeault offsets.
   *
   * @return array
   *   An array of default offsets.
   */
  public function getDefaultOffsets() : array {
    // Offsets.
    return [
      0 => '0 Columns',
      1 => '1 Columns',
      2 => '2 Columns',
      3 => '3 Columns',
      4 => '4 Columns',
      5 => '5 Columns',
      6 => '6 Columns',
      7 => '7 Columns',
      8 => '8 Columns',
      9 => '9 Columns',
      10 => '10 Columns',
      11 => '11 Columns',
    ];
  }

}
