<?php

namespace Drupal\foundation_layouts\Plugin\Layout;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Layout\LayoutDefault;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Layout class for Foundation Layouts.
 */
class FoundationLayouts extends LayoutDefault implements ContainerFactoryPluginInterface {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Config Breakpoints.
   *
   * @var array
   */
  protected $configBreakpoints;

  /**
   * Layouts configuration.
   *
   * @var array
   */
  protected $layoutsConfig;

  /**
   * Constructs a new class instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Config factory service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ConfigFactoryInterface $config_factory) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->configFactory = $config_factory;
    $this->layoutsConfig = $this->configFactory->get('foundation_layouts.settings');
    $this->configBreakpoints = $this->getBreakpoints();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    /** @var \Drupal\Core\Config\ConfigFactoryInterface $config_factory */
    $config_factory = $container->get('config.factory');
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $config_factory
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return parent::defaultConfiguration() + [
      'wrappers' => [],
      'wrapper_classes' => '',
      'wrapper_container' => TRUE,
      'wrapper_gutters' => [
        'x' => 'grid-padding-x',
      ],
      'sizes' => [],
      'order' => [],
      'offsets' => [],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    $configuration = $this->getConfiguration();
    $regions = $this->getPluginDefinition()->getRegions();
    $breakpoints = $this->configBreakpoints;
    $region_sizes = $this->getRegionSizes();
    $region_offsets = $this->getRegionOffsets();
    $region_orders = [];
    for($i = 0; $i < count($regions); $i++) {
      $region_orders[$i] = $i;
    }

    $form['attributes'] = [
      '#group' => 'additional_settings',
      '#type' => 'details',
      '#title' => $this->t('Wrapper attributes'),
      '#description' => $this->t('Attributes for the outermost element.'),
      '#tree' => TRUE,
    ];

    $form['attributes']['wrapper_classes'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Wrapper classes'),
      '#description' => $this->t('Add additional classes to the outermost element. Note: use classes as provided by your theme.  Separate multiple class names with a space.'),
      '#default_value' => $configuration['wrapper_classes'],
      '#weight' => 1,
    ];

    $form['attributes']['wrapper_gutters'] = [
      '#type' => 'container',
    ];

    $form['attributes']['wrapper_gutters']['instructions'] = [
      '#markup' => '<div class="callout warning">' . $this->t('<p>Gutters are the space between the Regions in a Layout. Gutters can be applied as "Padding", "Margin", or "None".  Choose "None" to remove the space between regions.</p>  <p>For most cases, "Padding" is the best option.</p>') . '</div>',
      '#allowed_tags' => ['div', 'p'],
    ];

    $form['attributes']['wrapper_gutters']['x'] = [
      '#type' => 'select',
      '#title' => $this->t('X-axis gutters'),
      '#description' => $this->t('Choose how gutters will apply to regions on the X-axis.'),
      '#options' => [
        'none' => $this->t('None'),
        'grid-margin-x' => $this->t('Margin'),
        'grid-padding-x' => $this->t('Padding'),
      ],
      '#default_value' => $configuration['wrapper_gutters']['x'] ?? 'grid-padding-x',
    ];

    $form['attributes']['wrapper_gutters']['y'] = [
      '#type' => 'select',
      '#title' => $this->t('Y-axis gutters'),
      '#description' => $this->t('Choose how gutters will apply to regions on the Y-axis.'),
      '#options' => [
        'none' => $this->t('None'),
        'grid-margin-y' => $this->t('Margin'),
        'grid-padding-y' => $this->t('Padding'),
      ],
      '#default_value' => $configuration['wrapper_gutters']['y'] ?? 'grid-padding-y',
    ];
    
    $form['attributes']['wrapper_container'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Contained wrapper'),
      '#description' => $this->t('When checked, the Layout wil be contained to the max width set in the theme (1200px by default).  When the screen is wider than the max width, the regions in the Layout will be centered with space equally applied to the left and right sides.'),
      '#default_value' => $configuration['wrapper_container'] ?? TRUE,
      '#weight' => 0,
    ];

    $form['regions'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Region Configuration'),
      '#weight' => 3,
      '#tree' => TRUE,
    ];

    $form['regions']['instructions'] = [
      '#markup' => '<div class="callout warning">' . $this->t('<p>Customize each region\'s configuration.</p> <p><strong>Note:</strong> Layouts are consist of twelve (12) equal width columns.  The total number of columns for all regions, including any offsets, must total 12 or less.</p>') . '</div>',
      '#allowed_tags' => ['div', 'p', 'strong'],
    ];

    foreach ($regions as $region_name => $region_definition) {

      $form['regions'][$region_name] = [
        '#type' => 'details',
        '#title' => $this->t('Region: @region', ['@region' => $region_name]),
        '#description' => $this->t('Use the settings below to customize the behavior of the @region region when viewed on each of the following screen sizes.', ['@region' => $region_definition['label']]),
        '#group' => 'region_config',
        '#weight' => $i,
        '#tree' => TRUE,
        '#attributes' => [
          'data-region' => $region_name
        ],
      ];

      foreach($breakpoints as $bp_name => $bp_label) {

        $form['regions'][$region_name][$bp_name] = [
          '#type' => 'details',
          '#title' => $this->t('@label settings', ['@label' => $bp_label]),
          '#description' => $this->t('These settings apply to the @region region when viewed on @size sized screens.', [
            '@region' => $region_definition['label'],
            '@size' => $bp_label,
          ]),
          '#tree' => TRUE,
          '#attributes' => [
            'data-breakpoint' => $bp_name
          ],
        ];

        $form['regions'][$region_name][$bp_name]['size'] = [
          '#type' => 'select',
          '#title' => $this->t('@label columns', ['@label' => $bp_label]),
          '#description' => $this->t('Columns define the width or span of each region.  Choose the number of columns the @region region will span when viewed on @label screens.', [
            '@region' => $region_definition['label'],
            '@label' => $bp_label,
          ]),
          '#options' => $region_sizes,
          '#default_value' => $configuration['sizes'][$region_name][$bp_name] ?? 'auto',
          '#required' => TRUE,
        ];

        if (count($region_offsets) > 1) {
          $form['regions'][$region_name][$bp_name]['offset'] = [
            '#type' => 'select',
            '#title' => $this->t('@label offset', ['@label' => $bp_label]),
            '#description' => $this->t('Offsets are empty columns at the beginning of each region.  Choose the number of columns to offset the @region region when viewed on @label screens.', [
              '@region' => $region_definition['label'],
              '@label' => $bp_label,
            ]),
            '#options' => $region_offsets,
            '#default_value' => $configuration['offsets'][$region_name][$bp_name] ?? 0,
          ];
        }
        elseif (count($region_offsets) == 1) {
          $form['regions'][$region_name][$bp_name]['offset'] = [
            '#type' => 'hidden',
            '#value' => key($region_offsets),
          ];
        }
        else {
          $form['regions'][$region_name][$bp_name]['offset'] = [
            '#type' => 'hidden',
            '#value' => 0,
          ];
        }

        if (count($regions) > 1 && count($region_orders)) {
          $form['regions'][$region_name][$bp_name]['order'] = [
            '#type' => 'radios',
            '#title' => $this->t('@label order', ['@label' => $bp_label]),
            '#description' => $this->t('Regions with lower values for Order appear before columns with higher values. Choose the order of the @region region when viewed on @label screens.', [
              '@region' => $region_definition['label'],
              '@label' => $bp_label,
            ]),
            '#options' => $region_orders,
            '#default_value' => $configuration['orders'][$region_name][$bp_name] ?? array_search($region_name, array_keys($regions), TRUE),
            '#required' => TRUE,
          ];
        }
        elseif (count($region_orders) == 1) {
          $form['regions'][$region_name][$bp_name]['order'] = [
            '#type' => 'hidden',
            '#value' => key($region_orders),
          ];
        }
        else {
          $form['regions'][$region_name][$bp_name]['order'] = [
            '#type' => 'hidden',
            '#value' => 0,
          ];
        }

      }
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    $breakpoints = $this->configBreakpoints;
    $regions = $form_state->getValue('regions');
    foreach ($regions as $region_name) {
      foreach ($breakpoints as $bp => $breakpoint) {
        $column_count = $regions[$region_name][$bp]['size'] + $regions[$region_name][$bp]['offset'];
        if ($column_count > 12) {
          $form_state->setErrorByName("regions][$region_name][$bp]['size']", $this->t("The total columns, including offsets, can not total more than 12."));
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $breakpoints = $this->configBreakpoints;

    $this->configuration['attributes'] = $form_state->getValue('attributes');
    foreach (['wrapper_classes', 'wrapper_container', 'wrapper_gutters'] as $name) {
      $this->configuration[$name] = $this->configuration['attributes'][$name];
      unset($this->configuration['attributes'][$name]);
    }

    $regions = $form_state->getValue('regions');
    foreach ($regions as $region_name => $region_config) {
      foreach ($breakpoints as $bp => $breakpoint) {
        $this->configuration['sizes'][$region_name][$bp] = $region_config[$bp]['size'];
        $this->configuration['offsets'][$region_name][$bp] = $region_config[$bp]['offset'];
        $this->configuration['orders'][$region_name][$bp] = $region_config[$bp]['order'];
      }
    }
    parent::submitConfigurationForm($form, $form_state);
  }

  /**
   * Returns an array of breakpoints.
   *
   * @return array
   *   An array of enabled breakpoints.
   */
  public function getBreakpoints() : array {
    $config = $this->layoutsConfig->get('breakpoints');
    $breakpoints = [
      'small' => $this->t('Small'),
      'medium' => $this->t('Medium'),
      'large' => $this->t('Large'),
      'xlarge' => $this->t('Extra large'),
      'xxlarge' => $this->t('Extra extra large'),
    ];

    $available_breakpoints = array_filter($config, function ($a) {
      return $a !== FALSE;
    });

    return array_intersect_key($breakpoints, $available_breakpoints);
  }

  /**
   * Returns an array of region_sizes.
   *
   * @return array
   *   An array of region sizes.
   */
  private function getRegionSizes() : array {
    $config = $this->layoutsConfig->get('available_sizes');
    // Column sizes.
    $region_sizes = [
      'auto' => $this->t('Auto'),
      1 => $this->t('One Column'),
      2 => $this->t('Two Columns'),
      3 => $this->t('Three Columns'),
      4 => $this->t('Four Columns'),
      5 => $this->t('Five Columns'),
      6 => $this->t('Six Columns'),
      7 => $this->t('Seven Columns'),
      8 => $this->t('Eight Columns'),
      9 => $this->t('Nine Columns'),
      10 => $this->t('Ten Columns'),
      11 => $this->t('Eleven Columns'),
      12 => $this->t('Twelve Columns'),
    ];
    $available_sizes = array_filter($config, function ($a) {
      return $a !== FALSE;
    });
    return array_intersect_key($region_sizes, $available_sizes);
  }

  /**
   * Returns an array of region_offsets.
   *
   * @return array
   *   An array of region offsets.
   */
  private function getRegionOffsets() : array {
    $config = $this->layoutsConfig->get('available_offsets');
    // Offsets.
    $region_offsets = [
      0 => $this->t('0 Columns'),
      1 => $this->t('1 Column'),
      2 => $this->t('2 Columns'),
      3 => $this->t('3 Columns'),
      4 => $this->t('4 Columns'),
      5 => $this->t('5 Columns'),
      6 => $this->t('6 Columns'),
      7 => $this->t('7 Columns'),
      8 => $this->t('8 Columns'),
      9 => $this->t('9 Columns'),
      10 => $this->t('10 Columns'),
      11 => $this->t('11 Columns'),
    ];

    $available_offsets = [];
    foreach ($region_offsets as $key => $value) {
      if ((bool) $config[$key] == TRUE) {
        $available_offsets[$key] = $value;
      }
    }
    return $available_offsets;
  }

}
