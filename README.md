# Foundation Layouts

This module provides a flexible, responsive grid layout plugin based on
 [Zurb Foundation](https://get.foundation). The module does not include nor 
does it install the Foundation library, instead, it assumes you are using 
Foundation in your theme. If you are looking for a Foundation-based theme, 
please checkout [Zurb Foundation](https://www.drupal.org/project/zurb_foundation).

**NOTE:** This project is not affiliated with [Foundation Layouts](https://www.drupal.org/project/foundation_layouts).

## Requirements 

1. A theme that includes Foundation 6 with the XY Grid enabled.
2. Core Layout Builder, or another module that utilizes layouts (such as [Layout Paragraphs](https://www.drupal.org/project/layout_paragraphs), [Display Suite](https://www.drupal.org/project/ds), [Panels](https://www.drupal.org/project/panels)).

## Features

The module includes four customizable layouts: 1 column, 2 columns, 3 columns, 
4 columns.  

Using the optional submodule, foundation_layouts_ui, the available grids, 
offsets, column orders, and available breakpoints can be customized to give 
editors / site builders as much or as little control over the layouts as 
desired.  

**NOTE:** It is recommended to disable  foundation_layouts_ui in production.

## Configuration

After enabling foundation_layouts and foundation_layouts_ui, visit
`/admin/config/content/foundation_layouts` to configure the available 
breakpoints, column widths, and offsets.

For column widths, it is often advisable to include the `Auto` option. When 
configuring layouts, the default option is `Auto`, which will make multi-region
 layouts equal with, which is likely the what an editor would expect.

After configuring the available options, ensure the Foundation Layouts are 
available to the module which will utilize them.  The default behavior for most
 modules is that all layouts are enabled, but if only specific layouts are, it 
may be necessary to update which layouts are available.

## Usage

When a Foundation Layout plugin is selected, each region presents the enabled 
options per enabled breakpoint.  This allows the editor to finely configure the
 layouts as desired.

If a attribute only has one option, it is hidden to simplify the Layout.


